package service;

import api.Api;
import api.ApiImpl;
import service.interfaces.Singleton;

public class ApiImplLazy implements Singleton {


    // Instance sadece static tanımlanır, üretilmez. (default değeri = null olur)
    private static ApiImplLazy intance;


    /*
        - Burada instance null ise, üretilir. Değilse sistemde yüklü olan return edilir √√

        - Aynı t zamanında instance almak isteyen 'iki farklı thread', içlerinde 'iki farklı singleton olan objeler' alıcaklardır.
          Dolayısıyla singleton mekanizması kırılır.


        - Bu şekilde instance çağırıldığı yerden, çağırıldığı anda oluşturulacaktır.
          Böylece ihtiyaç instance olmadıkça oluşturulmayacaktır.
          // bu case şöyle anlaşılabilir.
          Class içinde bir sabit, instance vs.. herhangi bir static field oluşturulur ve dışarıdan çağırılrsa,
          Class sisteme yüklenicektir. Ancak yinede class'ın bir objesi oluşturulmayacaktır.
     */
    public static ApiImplLazy getInstance() {

        if (intance == null) {

            intance = new ApiImplLazy();
        }

        return intance;
    }

    //----------------------------------------------------


    // Define property ( Type variable; )
    private Api api;

    private ApiImplLazy() {

        init();

    } // ..instantiation from outside - - -


    // Create property ( ..with init() method )
    private void init() {

        api = new ApiImpl();
    }

    // Get property ( Type getProperty() method )
    public Api getApi() {

        return api;
    }
}
