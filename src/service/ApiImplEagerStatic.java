package service;

import api.Api;
import api.ApiImpl;
import service.interfaces.Singleton;

/**
 *      - Bu durumda, instance constructor'a erişmeden önce çeşitli işlemler yapılabilir.
 *        (Dolayısı bir nevi static Factory method görevindedir.)
 *      - Aynı zamanda üretim aşamasında Exception Handling istenen durumlar için idealdir.
 *
 *      - - - Class sisteme yüklendiği anda 'instance' sisteme yüklenecektir √√
 *
 */
public class ApiImplEagerStatic implements Singleton {

    // Instance sadece static tanımlanır, üretilmez. (default değeri = null olur)
    private static ApiImplEagerStatic intance;

    static {

        try {

            intance = new ApiImplEagerStatic();

        } catch (Exception e) { // .. Herhangi spesific Exception atılabilir veya susturulabilir.

            throw new RuntimeException("Üretim yapılamadı.");
            // e.printStackTrace();
        }
    }

    public static ApiImplEagerStatic getInstance() {

        return intance;
    }

    //----------------------------------------------------
    //----------------------------------------------------


    // Define property ( Type variable; )
    private Api api;

    private ApiImplEagerStatic() {

        init();

    } // ..instantiation from outside - - -


    // Create property ( ..with init() method )
    private void init() {

        api = new ApiImpl();
    }

    // Get property ( Type getProperty() method )
    public Api getApi() {

        return api;
    }
}
