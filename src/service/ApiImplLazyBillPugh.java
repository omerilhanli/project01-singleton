package service;

import api.Api;
import api.ApiImpl;
import service.interfaces.Singleton;

public class ApiImplLazyBillPugh implements Singleton {

    /*
            - Instance kullanılacağı zaman çağırılıcağından, sisteme çağırıldığı anda yüklenecektir.
              Bill Pugh Lazy olarak instance üretme işlemini, Double Lock Mechanism gerekmeden Eager initialize mantığında
              halletmektedir. Bu sayede daha temiz code yazmış oluruz.

     */
    public static ApiImplLazyBillPugh getInstance() {

        return InstanceCreator.instance;
    }

    private static final class InstanceCreator {

        private static ApiImplLazyBillPugh instance = new ApiImplLazyBillPugh();
    }

    //----------------------------------------------------


    // Define property ( Type variable; )
    private Api api;

    private ApiImplLazyBillPugh() {

        init();

    } // ..instantiation from outside - - -


    // Create property ( ..with init() method )
    private void init() {

        api = new ApiImpl();
    }

    // Get property ( Type getProperty() method )
    public Api getApi() {

        return api;
    }
}
