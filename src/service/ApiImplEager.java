package service;

import api.Api;
import api.ApiImpl;
import service.interfaces.Singleton;

/**
 * ..Bu yapıda class, Singleton Pattern'inin Eager tekniği ile hızlıca üretilir.
 *
 *      - Resource kaybı gözlenmeyecek durumlar için idealdir.
 *
 *      * Kullanılması beklenen durumlar
 *          - TODO
 *      * Kullanılmaması istenen durumlar
 *          - TODO
 */

public class ApiImplEager implements Singleton {

    // Class sisteme yüklendiği anda 'instance' sisteme yüklenecektir √√

    private static ApiImplEager intance = new ApiImplEager();

    public static ApiImplEager getInstance() {

        return intance;
    }

    //----------------------------------------------------
    //----------------------------------------------------


    // Define property ( Type variable; )
    private Api api;

    private ApiImplEager() {

        init();

    } // ..instantiation from outside - - -


    // Create property ( ..with init() method )
    private void init(){

        api=new ApiImpl();
    }

    // Get property ( Type getProperty() method )
    public Api getApi() {

        return api;
    }
}
