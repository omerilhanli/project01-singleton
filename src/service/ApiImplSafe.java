package service;

import api.Api;
import api.ApiImpl;
import service.interfaces.Singleton;

/**
 *   - Joshua Bloch'un Effective Java kitabında önerdiği,
 *     hem eager, hem thread safe, hem de diğer singleton kıran mekanizmalara karşı güvenli enum yöntemi.
 *
 *   - Bu yöntem ile üretilen INSTANCE, static olarak 'new' lenicektir.
 *     Dolayısıyla istenen yerden çağırıldığında, Enum class'ı sisteme yüklendiği anda yüklenecektir.
 *     // Bu case şöyle anlaşılabilir,
 *     Farklı bir enum değişkeni tanımlandığı ve çağırıldığı anda, INSTANCE'da sisteme yüklenmiş olacaktır.
 *
 *   - Enum yönteminin bilinen dez avantajlarından biri Lazy yapılamama, yine Exception Handling yapılamama sorunlarıdır.
 *
 *   - Singleton yapısını kıran tekniklerden Serilization/Deserilization için alınması gereken mekanizma önlemlerini
 *     'dil düzeyinde default' olarak sağlamaktadır.
 *   - Yine multi thread işlemlerde singleton yapısını kıran durumlar için gereken mekanizma olan double lock mekanizma desteğini
 *     'dil düzeyinde default' olarak sağlamaktadır.
 *   - Yine Enum mekanizması, Reflection ile singleton instance'dan farklı instance'lar üreterek
 *     mekanizmayı kırma tekniğine karşı 'dil düzeyinde default olarak koruma' sağlamaktadır.
 *     // bunu default olarak private olan constructor'u ile sağlamaktadır. Reflection yöntemi ile
 *        constructora erişim visible edilememektedir.
 *
 *   - Dolayısıyla bir obje eager isteniyorsa başvurulması gereken en önemli singleton (mekanizması) tekniğidir.
 *
 */
public enum ApiImplSafe implements Singleton {


    INSTANCE;


    // Define property ( Type variable; )
    private Api api;

    ApiImplSafe() {

        init();
    }

    // Create property ( ..with init() method )
    private void init() {

        api = new ApiImpl();
    }

    // Get property ( Type getProperty() method )
    public Api getApi() {

        return api;
    }

}
