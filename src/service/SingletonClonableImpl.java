package service;

public class SingletonClonableImpl implements Cloneable {

    // public instance initialized when loading the class
    public static SingletonClonableImpl instance = new SingletonClonableImpl();

    private SingletonClonableImpl() {
        // private constructor
    }

    /*
                        * * * Bu durumda yeni bir object instance üretilmesi istenmez, üretilmiş instance return edilir.
                        *
                        * RESULT

                          -   instance1.hashCode(): 1300109446
                              instance2.hashCode(): 1300109446
     */
//    @Override
//    public Object clone() throws CloneNotSupportedException {
//
//        return instance;
//    }


    /*
                    * * * Bu durumda yeni bir object instance üretilmesi istenmez, Exception return edilir.
                    *
                    * RESULT

                         -  java.lang.CloneNotSupportedException at service.SingletonClonableImpl.clone...
    */
//    @Override
//    public Object clone() throws CloneNotSupportedException {
//
//        throw new CloneNotSupportedException();
//    }


    /*
                    * * * Default clone override, bu durumda yeni bir object instance return edilir. Singleton bozulur.
                    *
                    * RESULT

                      -   instance1.hashCode(): 1300109446
                          instance2.hashCode(): 1020371697
     */
    @Override
    public Object clone() throws CloneNotSupportedException {

        return super.clone();
    }
}
