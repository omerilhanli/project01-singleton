package service;

// Static Eager with Serializable singleton class

import api.Api;

import java.io.Serializable;

public class SingletonSerializableImpl implements Serializable {

    private static final long serialVersionUID = -6319681313133905496L;

    public static SingletonSerializableImpl instance = new SingletonSerializableImpl();

    private SingletonSerializableImpl() {

    }

    /*
            - Bu method ile ObjectInputStream içindeki default readResolve() metodu ezilir ve
              default obje instance'ı yerine burdaki obje instance'ı return edilir.


     */
    public Object readResolve() {

        return instance;
    }
}
