package service;

import api.Api;
import api.ApiImpl;
import service.interfaces.Singleton;

public class ApiImplLazyDoubleLock  implements Singleton {


    // Instance sadece static tanımlanır, üretilmez. (default değeri = null olur)
    private static ApiImplLazyDoubleLock intance;


    /**
     *   - Burada instance null ise, üretilir. Değilse sistemde yüklü olan return edilir √√
     *
     *   - 'synchronized' keywordu ile iki farklı thread instance almak istediği anda,
     *     ilk instance'ı almak isteyen thread 'new' statement'ına eriştiğinde, sonraki thread bekletilir.
     *     İlk thread block'tan çıktığında ikinci thread *ikinci null check'le karşılaşır.
     *     Sonuç, null olmayan bir instance'ın sistemde yüklü olmasından ikinci thread, 'new' statement'ına erişemez.
     *     Aynı instance ikinci thread'e de return edilir.
     *     // Bu mekanizmaya Double Lock Mechanism, veya Double Check Mechanism denmektedir.
     *
     *
     *   - Bu şekilde instance çağırıldığı yerden, çağırıldığı anda oluşturulacaktır.
     *     Böylece ihtiyaç instance olmadıkça oluşturulmayacaktır.
     *     // bu case şöyle anlaşılabilir.
     *     Class içinde bir sabit, instance vs.. herhangi bir static field oluşturulur ve dışarıdan çağırılrsa,
     *     Class sisteme yüklenicektir. Ancak yinede class'ın bir objesi oluşturulmayacaktır.
     *
     * @return
     */
    public static ApiImplLazyDoubleLock getInstance() {

        if (intance == null) {

            synchronized (ApiImplLazyDoubleLock.class){

                if (intance == null) {

                    intance = new ApiImplLazyDoubleLock();
                }
            }
        }

        return intance;
    }

    //----------------------------------------------------


    // Define property ( Type variable; )
    private Api api;

    private ApiImplLazyDoubleLock() {

        init();

    } // ..instantiation from outside - - -


    // Create property ( ..with init() method )
    private void init() {

        api = new ApiImpl();
    }

    // Get property ( Type getProperty() method )
    public Api getApi() {

        return api;
    }
}
