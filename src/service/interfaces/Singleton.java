package service.interfaces;

import api.Api;

public interface Singleton {

    Api getApi();
}
