package APPLICATION;

import client.*;
import service.SingletonClonableImpl;
import service.interfaces.Singleton;
import service.SingletonSerializableImpl;

public class Program {

    public static void main(String[] args) {

        Program p = new Program();

        //p.start(Runner.EAGER);

        //p.start(Runner.EAGER_STATIC);
        //p.start(Runner.LAZY);
        //p.start(Runner.LAZY_BILL_PUGH);
        //p.start(Runner.LAZY_DOUBLE_LOCK);
        //p.start(Runner.SAFE_WITH_ENUM);


        //p.newInstanceWithReflection();

        //p.newInstanceWithSerialization();

        p.newInstanceWithClonable();
    }

    public void start(Runner runner) {

        Client client = new Client(runner);

        client.work();
    }

    public void newInstanceWithReflection() {

        ClientReflection client = new ClientReflection();

        Singleton singleton1 = client.takeInstance();

        System.out.println("singleton1.hashCode(): " + singleton1.hashCode());

        Singleton singleton2 = client.takeInstance();

        System.out.println("singleton2.hashCode(): " + singleton2.hashCode());
    }

    public void newInstanceWithSerialization() {

        ClientSerilization clientSerilization = new ClientSerilization();

        SingletonSerializableImpl[] array = clientSerilization.takeInstance();

        System.out.println("singleton1.hashCode(): " + array[0].hashCode());

        System.out.println("singleton2.hashCode(): " + array[1].hashCode());
    }

    public void newInstanceWithClonable() {

        ClientClonable clientClonable = new ClientClonable();

        SingletonClonableImpl instance1 = clientClonable.takeInstance();

        SingletonClonableImpl instance2 = clientClonable.takeInstance();

        System.out.println("instance1.hashCode(): " + instance1.hashCode());

        System.out.println("instance2.hashCode(): " + instance2.hashCode());
    }

}