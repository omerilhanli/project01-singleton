package client;

import api.Constants;
import service.*;
import service.interfaces.Singleton;

public class Client {

    private Runner runner;

    public Client(Runner runner) {

        this.runner = runner;
    }

    public void work() {

        switch (runner) {

            case EAGER:

                ApiImplEager eager = ApiImplEager.getInstance();

                perform(eager);

                break;

            case EAGER_STATIC:

                ApiImplEagerStatic eagerStatic = ApiImplEagerStatic.getInstance();

                perform(eagerStatic);

                break;

            case LAZY:

                ApiImplLazy apiImplLazy = ApiImplLazy.getInstance();

                perform(apiImplLazy);

                break;

            case LAZY_BILL_PUGH:

                ApiImplLazyBillPugh apiImplLazyBillPugh = ApiImplLazyBillPugh.getInstance();

                perform(apiImplLazyBillPugh);

                break;


            case LAZY_DOUBLE_LOCK:

                ApiImplLazyDoubleLock apiImplLazyDoubleLock = ApiImplLazyDoubleLock.getInstance();

                perform(apiImplLazyDoubleLock);

                break;

            case SAFE_WITH_ENUM: // own perform implementation

                String g = ApiImplSafe.INSTANCE.getApi().getDataFromService(Constants.AUTH);

                System.out.println(g + " " + runner.name());

                break;

            default:
                break;
        }
    }


    void perform(Singleton instance) {

        String dataFromService = instance.getApi().getDataFromService(Constants.AUTH);

        System.err.println(dataFromService + " " + runner.name());
    }

}
