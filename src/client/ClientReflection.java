package client;

import service.ApiImplEager;
import service.interfaces.Singleton;

import java.lang.reflect.Constructor;

public class ClientReflection {

    public ClientReflection() {

    }

    /**
     *  - Enum singleton mekanizması hariç, Geri kalan tüm singleton mekanizmaları reflection ile kırılabilir.
     *
     * @return
     */
    public Singleton takeInstance() {

        try {

            Constructor[] constructors = ApiImplEager.class.getDeclaredConstructors();

            for (Constructor constructor : constructors) {

                // Below code will destroy the singleton pattern
                constructor.setAccessible(true);

                Singleton singleton = (Singleton) constructor.newInstance();

                return singleton;
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }
}
