package client;

import service.SingletonSerializableImpl;

import java.io.*;

/*
        - RESULTS

        * Object readResolve() metodu, NOT OVERRIDE ise

            instance1 hashCode(): 1338668845
            instance2 hashCode(): 159413332

        * Object readResolve() metodu, WITH OVERRIDE ise

            singleton1.hashCode(): 640070680
            singleton2.hashCode(): 640070680

 */
public class ClientSerilization {

    public SingletonSerializableImpl[] takeInstance() {

        SingletonSerializableImpl[] array = new SingletonSerializableImpl[2];

        try {

            SingletonSerializableImpl INSTANCE = SingletonSerializableImpl.instance;

            SingletonSerializableImpl INSTANCE01;

            SingletonSerializableImpl INSTANCE02;

            String pathname;

            // Mac OSX path. (Dil: Türkçe)
            //pathname = "/Kullanıcılar/kullanıcı/Masaustu/" + "dosya.uzanti";

            // Mac OSX path. (Language: English)
            //pathname = "/Users/user/Desktop/" + "file.extension";


            pathname = "/Users/omerilhanli/Desktop/" + "dosya.txt";

            // Windows path. (Dil: Türkçe)
            //pathname = "C:\\Klasor\\" + "dosya.uzanti";

            // Windows path. (Language: English)
            //pathname = "C:\\Folder\\" + "file.extension";


            File file = new File(pathname);


            FileOutputStream fileOut = new FileOutputStream(file);

            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(INSTANCE);

            out.close();

            fileOut.close();


            System.out.println("Serialized data is saved");


            FileInputStream fileIn1 = new FileInputStream(file);

            FileInputStream fileIn2 = new FileInputStream(file);

            ObjectInputStream in1 = new ObjectInputStream(fileIn1);

            ObjectInputStream in2 = new ObjectInputStream(fileIn2);


            INSTANCE01 = (SingletonSerializableImpl) in1.readObject();

            INSTANCE02 = (SingletonSerializableImpl) in2.readObject();


            System.out.println("Serialized data is deserialized and getting");


            array[0] = INSTANCE01;

            array[1] = INSTANCE02;


            in1.close();

            in2.close();

            fileIn1.close();

            fileIn2.close();

            return array;

        } catch (Exception tion) {

            tion.printStackTrace();
        }

        return null;
    }

}

