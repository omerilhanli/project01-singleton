package client;

import service.SingletonClonableImpl;

public class ClientClonable {

    public SingletonClonableImpl takeInstance() {

        try {

            return (SingletonClonableImpl) SingletonClonableImpl.instance.clone();

        } catch (Exception e) {

            e.printStackTrace();
        }

        return null;
    }

}
