package client;

public enum Runner {


    EAGER,


    EAGER_STATIC,


    LAZY,


    LAZY_BILL_PUGH,


    LAZY_DOUBLE_LOCK,


    SAFE_WITH_ENUM


}
