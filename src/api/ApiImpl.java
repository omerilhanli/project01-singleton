package api;

public class ApiImpl implements Api {

    @Override
    public String getDataFromService(String auth) {

        // Buraya gelmeden önce auth header'a eklenir...
        // Örneğin, parametre Retrofit library'si kullanılırken @Header annotation'ı ile işaretlenirse
        // auth (aslında token) header'a eklenir.

        // Remote servise bağlanılır, get işlemi ile datalar çekilir. Response client'a return edilir.
        String data = "...Datalar uzaktaki servisten alındı √√";

        return data;
    }
}
